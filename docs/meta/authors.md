# Authors

---

## Original Author

- Giordon Stark (@gstark) [:material-web:](https://giordonstark.com)
  [:material-github:](https://github.com/kratsg)
  [:material-twitter:](https://twitter.com/kratsg)

## Contributors

- Karol Krizka (@kkrizka)
- Bruce Gallop
